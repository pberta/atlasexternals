# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration options for building the AnalysisBase externals. Collected
# into a single place.
#

# Look for appropriate externals:
find_package( PythonInterp 2.7 QUIET )
find_package( Boost 1.58 QUIET )
find_package( ROOT 6.02.12 QUIET )

# Decide whether to build Python:
set( _flag FALSE )
if( NOT PYTHONINTERP_FOUND )
   set( _flag TRUE )
endif()
if( APPLE )
   # For macOS check whether we're picking up the Python version provided
   # by the system.
   get_filename_component( _pythonDir ${PYTHON_EXECUTABLE} DIRECTORY )
   if( ${_pythonDir} STREQUAL "/usr/bin" )
      # Since that's not appropriate for us... :-(
      set( _flag TRUE )
      # Make CMake forget that it "found" Python. As it will break the
      # environment setup of the AnalysisBaseExternals project now that
      # we're building Python as a part of it.
      get_property( _packages GLOBAL PROPERTY PACKAGES_FOUND )
      list( REMOVE_ITEM _packages PythonInterp )
      set_property( GLOBAL PROPERTY PACKAGES_FOUND ${_packages} )
      unset( _packages )
   endif()
   unset( _pythonDir )
endif()
option( ATLAS_BUILD_PYTHON
   "Build Python as part of the release" ${_flag} )

# Decide whether to build Boost:
set( _flag FALSE )
if( NOT Boost_FOUND OR ATLAS_BUILD_PYTHON )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_BOOST
   "Build Boost as part of the release" ${_flag} )

# Decide whether to build ROOT:
set( _flag FALSE )
if( NOT ROOT_FOUND OR ATLAS_BUILD_PYTHON )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_ROOT
   "Build ROOT as part of the release" ${_flag} )

# Clean up:
unset( _flag )
