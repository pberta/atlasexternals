AnalysisBaseExternals
=====================

This project builds all the externals needed by the standalone AnalysisBase
analysis release of ATLAS.

Many externals for AnalysisBase can be taken from the build system itself.
The project will not force building all of its externals by default if an
appropriate version of that external is already available on the build
machine. For instance for ROOT or Boost.

Configuration Options
---------------------

The configuration of the project respects the following options, all
defined in the `ProjectOptions.cmake` file:

   * `ATLAS_BUILD_PYTHON`: When turned on, the project builds an
   appropriate version of Python.
   * `ATLAS_BUILD_BOOST`: When turned on, the project builds an
   appropriate version of Boost.
   * `ATLAS_BUILD_ROOT`: When turned on, the project builds an
   appropriate version of ROOT.

The project will try to set correct default values for these variables
based on the build environment. But they can be overridden during the
CMake configuration if necessary.
