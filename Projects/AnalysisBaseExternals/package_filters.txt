# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AnalysisBaseExternals.
#
+ External/BAT
+ External/Boost
+ External/Eigen
+ External/FastJet
+ External/FastJetContrib
+ External/GoogleTest
+ External/HEPUtils
+ External/Lhapdf
+ External/MCUtils
+ External/PyAnalysis
+ External/Python
+ External/ROOT
- .*
