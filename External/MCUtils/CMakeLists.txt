# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building MCUtils as part of the offline/analysis software.
#

# Declare the name of the package:
atlas_subdir( MCUtils )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The source code of MCUtils:
set( _mcutilsSource "http://cern.ch/service-spi" )
set( _mcutilsSource "${_mcutilsSource}/external/tarFiles/" )
set( _mcutilsSource "${_mcutilsSource}MCGeneratorsTarFiles/" )
set( _mcutilsSource "${_mcutilsSource}mcutils-1.2.1.tar.gz" )
set( _mcutilsMd5 "7c60cc91553e8d889fdb7dc1147407a7" )

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/MCUtilsBuild )

# "Build" MCUtils:
ExternalProject_Add( MCUtils
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_mcutilsSource}
   URL_MD5 ${_mcutilsMd5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the installation of MCUtils"
   BUILD_COMMAND make install PREFIX=${_buildDir}
   INSTALL_COMMAND make install PREFIX=<INSTALL_DIR> )
add_dependencies( Package_MCUtils MCUtils )

# Install MCUtils:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
